import { useState } from "react";
import ReCAPTCHA from "react-google-recaptcha";

import money from "@functions/money";

import cart from "@store/cart";

const Index = () => {
  const [data, setData] = useState({
    name: "",
    phone: "",
    address: "",
  });

  const changeData = (id) => (e) => {
    var newValue = e;
    if (e.target) {
      newValue = e.target.value;
    }
    setData({
      ...data,
      [id]: newValue,
    });
  };
  const validateData = () => {
    if (data.name.split(" ").join("") == "") {
      alert("Nombre invalido");
      return false;
    }
    if (data.phone.split(" ").join("") == "") {
      alert("Nombre invalido");
      return false;
    }
    if (data.address.split(" ").join("") == "") {
      alert("Nombre invalido");
      return false;
    }
    return true;
  };

  const sendForm = async (e) => {
    e.preventDefault();
    if (validateData()) {
      const cart_ = cart.get();
      const url = `https://web.whatsapp.com/send?phone=+573223173104`;
      const msj = `Hola soy *${data.name}*, mi numero es *${data.phone}*, vivo en *${data.address}*, mi pedido es:`;
      const pedido = cart_
        .map((e) => {
          return `${e.count} de ${e.title} (precio ${money(e.prices.price)})`;
        })
        .join("\n");
      window.open(`${url}&text="${msj} ${pedido}"`, "_ blank");
    }
  };
  const handleRecaptchaChange = (value) => {
    console.log("Valor de reCAPTCHA:", value);
    // Puedes almacenar el valor del reCAPTCHA en el estado si necesitas enviarlo junto con el formulario
  };

  return (
    <>
      <div className="pedir bg-black-lite">
        <form className="pedir-form" onSubmit={sendForm}>
          <span className="pedir-title color color-white font-64 font-montserrat font-w-700">
            Pedir
          </span>
          <div className="pedir-content">
            <label className="pedir-form-info">
              <span className="font-18 color-white font-montserrat pedir-form-info-span">
                Nombre
              </span>
              <input
                value={data.name}
                onChange={changeData("name")}
                className="pedir-form-input font-16 font-montserrat"
                type="text"
                placeholder="Nombre"
                name="Nombre"
                required
              />
            </label>

            <label className="pedir-form-info">
              <span className="color-white font-18 font-montserrat pedir-form-info-span">
                Telefono
              </span>
              <input
                value={data.phone}
                onChange={changeData("phone")}
                className="pedir-form-input font-16 font-montserrat"
                placeholder="Telefono"
                type="tel"
                name="Telefono"
                required
              />
            </label>

            <label className="pedir-form-info ">
              <span className="color-white  font-18  font-montserrat pedir-form-info-span">
                Direccion
              </span>
              <input
                value={data.address}
                onChange={changeData("address")}
                className="pedir-form-input font-16 font-montserrat"
                type="text"
                placeholder="Direccion"
                name="Direccion"
                required
              />
            </label>
            <label className="pedir-form-info">
              <span className="color-white  font-18  font-montserrat pedir-form-info-span">
                ReCaptcha
              </span>
              <ReCAPTCHA
                sitekey="6Ld7VOclAAAAANNFHe3AmqeWhHeRVpG0N8yK0Jt3"
                onChange={handleRecaptchaChange}
              />
            </label>

            <button className=" pedir-form-buttom color-white bg-purple font-18 font-montserrat font-w-700 ">
              Pedir
            </button>
          </div>
        </form>
      </div>
    </>
  );
};
export default Index;
