import { useState } from "react"

import InputNumber from "@components/inputNumber"
import Source from "@components/sourceimg"

import money from "@functions/money"

import cart from "@store/cart"

const Index = (props) => {
    const [countProduct, setCountProduct] = useState(1)
    const {
        id,
        title,
        img,
        text,
        prices,
    } = {
        id:1,
        title : "",
        img : {
            src:"",
            name:""
        },
        text:"",
        prices: {
            subPrice: 0,
            price: 0
        },

        ...props,
    }
    const addProduct = () => {
        cart.add(id,countProduct)
    }
    
    return (
        <>
            <article className="product flex">
                <picture className="product-picture flex-4">
                    <Source
                        className="product-picture-img" 
                        name={img.name}
                        img={img.src}
                    />
                </picture>
                <div className="product-info bg-soft-purple-2 flex-8">
                    <div className="product-info-top">
                        <h2 className="product-info-title font-48 color-red font-italiana">
                            {title}
                        </h2>
                        <p className="product-info-text font-montserrat font-24">
                            {text}
                        </p>
                    </div>
                    <div className="product-info-content">
                        <div className="product-info-price">
                            <span className="subPrice color-black font-montserrat font-36">
                                {money(prices.subPrice)}
                            </span>
                            <span className="price color-red font-montserrat font-45">
                                {money(prices.price)}
                            </span>
                        </div>
                        <div className="product-info-count ">
                            <InputNumber
                            className="product-info-count-content"
                            classNameInput="product-info-count-input font-montserrat bg-white color-red font-14"
                            min={1}
                            value={countProduct}
                            onChange={setCountProduct}
                            />
                            <button onClick={addProduct} className="product-info-btn bg-red color-white font-montserrat font-w-700 font-30 ">
                                Pedir
                            </button>
                        </div>
                    </div>
                </div>
            </article>
        </>
    )
}
export default Index