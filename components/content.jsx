import ReactGA from "react-ga";
import Header from "@components/header";
import Bg from "@components/background-img";
import Footer from "@components/footer";
import Cart from "@components/cart";
import { useState, useEffect } from "react";
import Head from "next/head";

const Index = ({ children }) => {
  const [height, setHeight] = useState(0);
  useEffect(() => {
    const header = document.getElementById("header");
    const hHeader = header.offsetHeight;

    const footer = document.getElementById("footer");
    const hFooter = footer.offsetHeight;

    const hHF = hHeader + hFooter;

    setHeight(`calc(100vh - ${hHF / 16}rem)`);
  }, []);

  useEffect(() => {
    ReactGA.initialize("G-EE9J2CHFNG");
    ReactGA.pageview(window.location.pathname);
  }, []);

  return (
    <>
      <Head>
        <title>QuieroCupcakes</title>
        <link
          rel="shortcut icon"
          type="image/png"
          href="/img/favicon.png"
        ></link>
      </Head>

      <Bg />
      <Cart />
      <Header />
      <div className="content-page" style={{ minHeight: height }}>
        <div className="content-page-childrens">{children}</div>
      </div>
      <Footer />
    </>
  );
};

export default Index;
