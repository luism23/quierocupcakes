import { useState } from "react"

const Arrow = (
    <svg width="1.5rem" height="1.375rem" viewBox="0 0 24 22" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="0" width="100%" height="100%" rx="5" fill="white" />
        <mask id="mask0_15_1512" style={{ maskType: "alpha" }} maskUnits="userSpaceOnUse" x="0" y="0" width="100%" height="100%">
            <rect x="0" width="100%" height="100%" rx="5" fill="white" />
        </mask>
        <g mask="url(#mask0_15_1512)">
            <path d="M10.2682 3.65838C11.0539 2.42134 12.8592 2.42134 13.6448 3.65838L20.3601 14.2321C21.2057 15.5637 20.2491 17.3043 18.6718 17.3043H5.24129C3.66393 17.3043 2.70735 15.5637 3.55299 14.2321L10.2682 3.65838Z" fill="currentColor" />
        </g>
    </svg>
)
const Index = (props) => {
    const { 
        className,
        classNameInput,
        value,
        onChange,
        min,
        max
    }={ 
        className : "", 
        classNameInput : "", 
        value : 1, 
        onChange : () => { }, 
        min : -99999999, 
        max : 9999999999,

        ...props
    }

    const changeValue = (e) => {
        var newValue = e
        if (e.target) {
            newValue = e.target.value
        }
        // value = Math.min(value,max)
        // value = Math.max(value,min)

        newValue = Math.max(Math.min(newValue, max), min)

        onChange(newValue)
    }
    return (
        <div className={className}>
            <input
                className={classNameInput}
                type="number"
                value={value}
                onChange={changeValue}
            />
            <div className="content-arrow">
                <div className="arrow arrow-up" onClick={() => { changeValue(value + 1) }}>
                    {Arrow}
                </div>
                <div className="arrow arrow-down" onClick={() => { changeValue(value - 1) }}>
                    {Arrow}
                </div>
            </div>
        </div>
    )
}
export default Index