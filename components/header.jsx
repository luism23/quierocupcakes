import { useState, useEffect } from "react";
import Link from "next/link";
import Source from "@components/sourceimg";
import navs from "@data/navs";

const LinkNav = ({ text = "", href = "/" }) => {
  return (
    <Link href={href}>
      <a className="color-white font-montserrat font-28 font-w-700">{text}</a>
    </Link>
  );
};

const Header = () => {
  const [cartCount, setCartCount] = useState(0); // Estado para el número de productos en el carrito
    console.log(cartCount)
  const toggleCart = () => {
    document.body.classList.toggle("cartActive");
  };

  useEffect(() => {
    // Obtener los productos del carrito desde el estado local (ejemplo)
    const cartItems = JSON.parse(localStorage.getItem("cart")) || [];
    setCartCount(cartItems.length);
    console.log(cartItems)
  }, []);

  return (
    <header id="header" className="header">
      <div className="container header-content flex flex-align-center">
        <picture className="flex-4">
          <Source img="header-logo.png" name="Logo" className="header-logo" />
        </picture>
        <nav className="header-nav flex-4 flex flex-justify-center">
          {navs.map((e, i) => {
            return <LinkNav key={i} {...e} />;
          })}
        </nav>
        <div className="header-content-cart flex-4 flex flex-justify-right">
          <div onClick={toggleCart} className="flex">
            <img
              className="img-cart"
              src="/svg/carrito.svg"
              alt="carrito"
            />
            {cartCount > 0 && (
              <div className="cart-count">{cartCount}</div>
            )}
            <button className="header-buttom bg-red"></button>
          </div>
        </div>
      </div>

      <style jsx>{`
        .cart-count {
          position: absolute;
          top: -10px;
          right: -10px;
          display: flex;
          align-items: center;
          justify-content: center;
          width: 18px;
          height: 18px;
          border-radius: 50%;
          background-color: red;
          color: white;
          font-size: 12px;
          font-weight: bold;
        }
      `}</style>
    </header>
  );
};

export default Header;
