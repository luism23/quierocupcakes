import { useState, useEffect } from "react"
import { useRouter } from 'next/router'
import Link from "next/link"

import InputNumber from "@components/inputNumber"
import Source from "@components/sourceimg"

import money from "@functions/money"

import cart from "@store/cart"


const toggleCart = () => {
    document.body.classList.toggle("cartActive")
}

const Product = (props) => {
    const {
        id,
        img,
        title,
        prices,
        count,
    } = {
        id: 0,
        img: {
            src: "",
            name: "",
        },
        title: "",
        prices: {
            subPrice: 0,
            price: 0
        },
        count: 1,

        ...props,
    }
    const [count_, setCount] = useState(count)
    const updateProduct = (e) => {
        cart.updateProduct(id, e)
        setCount(e)
    }
    const removeProduct = () => {
        cart.remove(id)
    }
    const loadCount = () => {
        const cart_ = cart.get()
        const thisProduct = cart_.find((e) => e.id == id)
        if (thisProduct != undefined) {
            setCount(thisProduct.count)
        }
    }
    useEffect(() => {
        var e = document.body
        var observer = new MutationObserver(function (event) {
            if (document.body.classList.value.indexOf("cartActive") != -1) {
                loadCount()
            }
        })

        observer.observe(e, {
            attributes: true,
            attributeFilter: ['class'],
            childList: false,
            characterData: false
        });
    }, [])
    return (
        <div className="cart-product bg-soft-purple-2">
            <picture>
                <Source
                    className="cart-product-img"
                    img={img.src}
                    name={img.name}
                />
            </picture>
            <div className="cart-product-info">
                <h4 className="cart-product-title color-red font-24 font-italina">
                    {title}
                </h4>
                <div className="cart-product-prices" >
                    <span className="subPrice  color-black font-montserrat font-16">
                        {money(prices.subPrice)}
                    </span>
                    <span className="price color-red font-montserrat font-20 ">
                        {money(prices.price)}
                    </span>
                </div>
            </div>
            <InputNumber
                className="product-info-count-content"
                classNameInput="product-info-count-input font-montserrat bg-white color-red font-14"
                min={1}
                value={count_}
                onChange={updateProduct}
            />
            <svg className="delete" onClick={removeProduct} width="1.375rem" height="1.375rem" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="18.3848" width="5" height="26" rx="2.5" transform="rotate(45 18.3848 0)" fill="var(--color-red)" />
                <rect y="3.53564" width="5" height="26" rx="2.5" transform="rotate(-45 0 3.53564)" fill="var(--color-red)" />
            </svg>
        </div>
    )
}


const Index = () => {

    const router = useRouter()
    const [products, setProducts] = useState([])
    const [total, setTotal] = useState(0)
    const updateCart = () => {
        const np = cart.get()
        setProducts(np)
        var newTotal = 0
        np.forEach(element => {
            newTotal += element.count * element.prices.price
        });
        console.log(np);
        if(np.length == 0){
            router.push("/")
            document.body.classList.remove("cartActive")
        }
        setTotal(newTotal)
    }
    useEffect(() => {
        updateCart()
        cart.setUpdate(() => {
            updateCart()
        })
    }, [])
    return (
        <>
            <div className="bg-gray-2 bg-cart-left"></div>
            <div className="cart bg-white">
                <div className="cart-top">
                    <h2 className="cart-title color-black font-montserrat font-24 font-w-700">
                        Carrito
                        <span className="total">
                            {money(total)}
                        </span>
                    </h2>
                    <div className={`cart-products ${products.length}`}>
                        {products.map((e, i) => {
                            return (
                                <Product key={i} {...e} />
                            )
                        })}
                    </div>
                </div>
                <div className="cart-buttoms">
                    <button onClick={toggleCart} className="cart-buttom color-white bg-red font-24 font-montserrat font-w-700">
                        Cancelar
                    </button>
                    <Link href="/pedir">
                        <a onClick={toggleCart} className="cart-buttom color-white bg-purple font-24 font-montserrat font-w-700 ">
                            Solicitar Pedido
                        </a>
                    </Link>
                </div>
            </div>
        </>
    )
}
export default Index