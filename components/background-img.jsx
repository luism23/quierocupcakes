import Source from "@components/sourceimg"

const Index = () => {
    return (
        <>
            <picture className="background-picture">
                <Source
                    img="fondo2.png"
                    name="fondo"
                    className="background-picture-img"
                />
            </picture>
        </>
    )
}
export default Index