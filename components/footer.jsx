import Source from "@components/sourceimg";

import contacts from "@data/contacts";

const FooterLink = ({ href = "", text = "", Img = "" }) => {
  return (
    <a
      href={href}
      rel="noopener"
      className="footer-content-info-link color-white font-montserrat font-14"
    >
      <img src={Img} alt="" />
      {text}
    </a>
  );
};

const FooterInfo = ({ title = "", links = [] }) => {
  return (
    <div className="footer-content-info">
      <h2 className="footer-content-info-title color-white font-montserrat font-18">
        {title}
      </h2>
      <div className="footer-content-info-linkcs">
        {links.map((e, i) => {
          return <FooterLink key={i} {...e} />;
        })}
      </div>
    </div>
  );
};

const Index = () => {
  return (
    <>
      <footer id="footer" className="footer">
        <div className="container footer-content flex">
          <picture className="content-img">
            <Source
              img="favicon.png"
              name="Logo Footer"
              className="footer-content-img"
            />
            <FooterInfo
              title="🧁Nuestro propósito es endulzar tu paladar!"
              links={contacts}
            />
          </picture>
        </div>
      </footer>
      <div className="bg-black fts">
        <span className="font-12 font-w-400 color-white text-center flex flex-justify-center font-montserrat">
             https://quierocupcakes.vercel.app/
        </span>
      </div>
    </>
  );
};
export default Index;
