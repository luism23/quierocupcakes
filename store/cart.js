import Cookies from 'js-cookie'
import products from "@data/products"

var updateCart = () => {}

const getCart = () => {
    var cart = Cookies.get('cart')
    if(!cart){
        cart = "[]"
    }
    cart = JSON.parse(cart)
    return cart
}
const postCart = (cart) => {
    Cookies.set('cart',JSON.stringify(cart))
    updateCart()
}
const addCart = (idProduct,count) => {
    var cart = getCart()
    if(cart.find((e)=>e.id === idProduct)){
        cart = cart.map((e)=>{
            if(e.id === idProduct){
                e.count += count
            }
            return e
        })
    }else{
        const newProduct = products.find((e)=>e.id === idProduct)
        newProduct.count = count
        cart.push(newProduct)
    }
    postCart(cart)
}
const updateProductCart = (idProduct,count) => {
    var cart = getCart()
    cart = cart.map((e)=>{
        if(e.id === idProduct){
            e.count = count
        }
        return e
    })
    postCart(cart)
}
const removeCart = (idProduct) => {
    var cart = getCart()
    cart = cart.filter((e)=> e.id !== idProduct)
    postCart(cart)
}

export default {
    get : getCart,
    add : addCart,
    remove : removeCart,
    setUpdate : (f) => {
        updateCart = f
    },
    updateProduct : updateProductCart
}