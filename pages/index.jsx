import Product from "@components/products";
import Content from "@components/content";

import products from "@data/products";

const Index = () => {
  return (
    <>
      <Content>
        <section className="banners flex bg-gray-2">
          <div className="flex container ">
            <div className="flex-6 flex flex-justify-center flex-direction-column font-montserrat font-30 color-red ">
              <h1 className="title-princ">
                Nuestro propósito es <br />
                endulzar tu paladar!
              </h1>
              <p className="font-16 color-black font-w-400">Postres/Dulces/Repostería</p>
              <button className="pedir-form-buttom color-white bg-purple font-18 font-montserrat font-w-700">
                Haz tu pedido Ahora
              </button>
            </div>
            <div className="flex-6 flex flex-justify-center">
              <img className="wafles" src="./img/Wafles.png" alt="" />
            </div>
          </div>
        </section>
        <section className="container">
          {products.map((e, i) => {
            return <Product key={i} {...e} />;
          })}
        </section>
      </Content>
    </>
  );
};
export default Index;
