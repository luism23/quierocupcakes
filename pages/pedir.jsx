import { useState, useEffect } from "react"
import { useRouter } from 'next/router'

import Pedidos from "@components/pedidos"
import Content from "@components/content"

import cart from "@store/cart"

const Index = () => {
    const router = useRouter()
    useEffect(()=>{
        const cart_ = cart.get()
        if(cart_.length == 0){
            router.push("/")
        }
    },[])
    return (
        <>
            <Content>
                <section className="container">
                    <Pedidos />
                </section>
            </Content>
        </>
    )
}
export default Index