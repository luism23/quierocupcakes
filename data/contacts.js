export default [
    {
        Img:"./svg/whatsapps.svg",
        href:"https://wa.link/2k3l3r",
        text:"Contacto de Whatsapp"
    },
    {
        Img:"./svg/location.svg",
        href:"https://goo.gl/maps/wCY4S4M9i21vYTgE7",
        text:"Ubicamos en Peribeca"
    },
    {
        Img:"./svg/instagram.svg",
        href:"https://www.instagram.com/quierocupcakes._/",
        text:"QuieroCupcakes._"
    },
    {
        Img:"./svg/envelope-circle-check-solid.svg",
        href:"mailto:quiero.cupcakes.oficial@gmail.com",
        text:"quiero.cupcakes.oficial@gmail.com"
    },
]



